new Vue({
    el: '#profesores',
    data:{
        title: 'Administración de Profesores',
        usuario: {},
        profesores: [],
        newProf:{
            "nombre": null,
            "apellido": null,
            "email": null,
            "DNI": null,
        },
        dataEditProf: {
            "nombre": null,
            "apellido": null,
            "email": null,
            "DNI": null,
        },
        dataDeleteProf: {
            "nombre": null,
            "apellido": null,
            "email": null,
            "DNI": null,
        },
    },
    methods: {
        async getUser(){
            await axios.get('backend/session.php')
            .then(resp=>{
                this.usuario=resp.data
                //console.log(this.usuario)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async getProfesores(){
            await this.getUser()
            data = {
                "role" : this.usuario.role
            }
            await axios.post('backend/getProfesores.php',data)
            .then(resp=>{
                this.profesores = resp.data
                console.log(this.profesores)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async saveProfesor(){
            await this.getUser()

            let data = this.newProf
            data.role = this.usuario.role
            data.idupdater = this.usuario.ID
            
            console.log(data)

            await axios.post('backend/addTeacher.php', data)
            .then(resp=>{
                let addteacher = resp.data
                if(addteacher.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${addteacher.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${addteacher.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getProfesores()
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.newProf={
                "nombre": null,
                "apellido": null,
                "email": null,
                "DNI": null,
            }
        },
        async editProfesor(){
            await this.getUser()
            console.log(this.dataEditProf)
            let data = {
                "ID":this.dataEditProf.ID,
                "nombre":this.dataEditProf.nombre,
                "apellido":this.dataEditProf.apellido,
                "email":this.dataEditProf.email,
                "dni":this.dataEditProf.DNI,
                "idupdater": this.usuario.ID,
                "role": this.usuario.role,
            }

            await axios.post('backend/editProfesor.php', data)
            .then(resp=>{
                let editTeacher = resp.data
                console.log(editTeacher)
                if(editTeacher.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${editTeacher.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${editTeacher.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getProfesores()
                }
            })
            .catch(e=>{
                console.log(e)
            })
            dataEditProf = {
                "nombre": null,
                "apellido": null,
                "email": null,
                "DNI": null,
            }
        },
        deleteProfesor(){
            console.log(this.dataDeleteProf)
            let data = {
                "ID":this.dataDeleteProf.ID,
                "role": this.usuario.role,
            }
            console.log(data)
            axios.post('backend/deleteTeacher.php',data)
            .then(resp=>{
                let deleteTeacher = resp.data
                console.log(deleteTeacher)
                if(deleteTeacher.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${deleteTeacher.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${deleteTeacher.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getProfesores()
                }
            })
            .catch(e=>{
                console.log(e)
            })
            dataDeleteProf = {
                "nombre": null,
                "apellido": null,
                "email": null,
                "DNI": null,
            }
        }
    },
    created() {
    },
    mounted() {
        this.getProfesores()
    }
});