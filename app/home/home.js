new Vue({
    el: '#home',
    data:{
        title: 'Bienvenidos a Mi Aula Virtual',  // de la EET N°21 Gral. Manuel Belgrano',
        usuario:{},
        menu: ''
    },
    methods: {
        getUser(){
            axios.get('backend/session.php')
            .then(resp=>{
                this.usuario=resp.data
            })
            .catch(e=>{
                console.log(e)
            })
        }
    },
    created() {
    },
    mounted() {
        this.getUser()
    },
});