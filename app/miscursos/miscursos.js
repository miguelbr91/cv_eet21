new Vue({
    el: '#miscursos',
    data:{
        title: 'Mis Cursos',
        usuario: {},
        cursos: [],
        alumnos_curso:[],
        profesores_curso:[],
        dataCourse:{},
        profAsigCurso:[],
        alumAsigCurso:[],
        verCurricula:false,
        idxCourse: null,
        curricula: [],
        selectCurricula:[],
        selectUnit: null,
        screenWidth: 0,
        mostrarAct: null
    },
    methods: {
        async getUser(){
            await axios.get('backend/session.php')
            .then(resp=>{
                this.usuario=resp.data
                //console.log(this.usuario)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async getMiCursos(){
            await this.getUser()
            data = {
                "role" : this.usuario.role,
                "idusers": this.usuario.ID
            }
            console.log(data)
            await axios.post('backend/getCursos.php', data)
            .then(resp=>{
                this.cursos = resp.data
                console.log(this.cursos)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async getProfesoresCurso(){
            await axios.get('backend/getTeacherForCourse.php')
            .then(resp=>{
                this.profesores_curso=resp.data
                console.log(this.profesores_curso)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async getAlumnosCurso(){
            await axios.get('backend/getStudentForCourse.php')
            .then(resp=>{
                this.alumnos_curso=resp.data
                console.log(this.alumnos_curso)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getViewCourse(){
            console.log(this.dataCourse)
            let idcurso = this.dataCourse.ID
            this.alumAsigCurso = this.alumnos_curso.filter(item => item.IDcurso === idcurso)
            this.profAsigCurso = this.profesores_curso.filter(item => item.IDcurso === idcurso)
            console.log('Alumnos:',this.alumAsigCurso)
            console.log('Profesores:',this.profAsigCurso)
        },
        async cerrarVistaCurso(){
            this.alumnos_curso = []
            this.profesores_curso = []
            this.dataCourse = {}
            this.profAsigCurso = []
            this.alumAsigCurso = []
            await this.getMiCursos()
            await this.getProfesoresCurso()
            await this.getAlumnosCurso()
        },
            // Metodos de Curricula
            async getCurricula(){
                this.idxCourse = this.dataCourse.ID
                let data = {
                    "idcourse" : this.idxCourse
                }
                axios.post('backend/getCurricula.php', data)
                .then(resp=>{
                    console.log(resp.data)
                    this.curricula = resp.data
                    this.selectCurricula = this.curricula.filter(item => item.nro === this.selectUnit )
                })
                .catch(e=>{
                    console.log(e)
                })
            },
            loadCurricula(){

                this.selectCurricula = this.curricula.filter(item => item.nro === this.selectUnit )
            },
            readScreenWidth(){
                this.screenWidth = document.documentElement.clientWidth
                console.log('Ancho pantalla',this.screenWidth)
            },
    },
    created() {
    },
    mounted() {
        this.getMiCursos()
        this.getProfesoresCurso()
        this.getAlumnosCurso()
        this.readScreenWidth()
    },
    updated() {
        
    },
});