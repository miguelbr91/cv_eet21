new Vue({
    el: '#cursos',
    data:{
        title: 'Administración de Cursos',
        cursos: [],
        usuario: {},
        profesores: [],
        alumnos: [],
        newCurso: {
            "nombre": null,
            "anio": null,
            "esp": null,
        },
        dataCourseDel: {
            "nombre": null,
            "anio": null,
            "esp": null,
        },
        dataCourseEdit: {
            "nombre": null,
            "anio": null,
            "esp": null,
            "especialidad": null,
        },
        dataAddProfCourse: {
            "teacher" : null,
            "course" : {
                "nombre": null,
                "anio": null,
                "esp": null,
            }
        },
        dataAddAlumCourse: {
            "student" : null,
            "course" : {
                "nombre": null,
                "anio": null,
                "esp": null,
            }
        },
        profesores_curso:[],
        alumnos_curso:[],
        profAsigCurso:[],
        alumAsigCurso:[],
        dataCourse:{},
        dataDeleteUserCourse: {},
            // Variables de curricula
            titleCurricula:'Administrar Contenido del Curso',
            cargaCurricula:false,
            curricula:[],
            unidad:{
                "idunits":null,
                "nombre":null,
                "nro":null,
                "descripcion":null,
            },
            actividad:{
                "idactivities": null,
                "nombre":null,
                "descripcion":null,
            },
            recurso:{
                "idresources":null,
                "url":null,
                "file":null,
                "type":null
            },
            dataSelectedCourse:{
                "ID": null,
                "nombre": null,
                "anio": null,
                "esp": null,
            },
            idunits:null,
            idactivities:null,
            idxCourse:null,
            idxUnidad:null,
            idxActividad:null,
            idxRecurso:null,
            dataDeleteRecurso:null,
            dataDeleteActividad:null,
            dataDeleteUnidad:{
                "idunits":null,
                "nombre":null,
                "nro":null,
                "descripcion":null,
            },
            uploadingFile:false,
    },
    methods: {
        async getUser(){
            await axios.get('backend/session.php')
            .then(resp=>{
                this.usuario=resp.data
                //console.log(this.usuario)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async getCursos(){
            await this.getUser()
            data = {
                "role" : this.usuario.role
            }
            await axios.post('backend/getCursos.php', data)
            .then(resp=>{
                this.cursos = resp.data
                console.log(this.cursos)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async getProfesores(){
            await this.getUser()
            data = {
                "role" : this.usuario.role
            }
            await axios.post('backend/getProfesores.php',data)
            .then(resp=>{
                this.profesores = resp.data
                console.log(this.profesores)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async saveCurso(){
            await this.getUser()
            let data = this.newCurso
            data.role = this.usuario.role
            console.log(data)
            await axios.post('backend/addCourse.php', data)
            .then(resp=>{
                console.log(resp.data)
                let addcourse = resp.data
                if(addcourse.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${addcourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${addcourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getCursos()
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async deleteCurso(id){
            console.log(this.dataCourseDel)
            let data = {
                "role" : this.usuario.role,
                "ID" : this.dataCourseDel.ID
            }
            console.log(data)
            await axios.post('backend/deleteCourse.php', data)
            .then(resp=>{
                let deletecourse = resp.data
                console.log(deletecourse)
                if(deletecourse.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${deletecourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${deletecourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getCursos()
                }
            })
            .catch(e=>{
                console.log(e)
            })
            dataCourseDel = {
                "nombre": null,
                "anio": null,
                "div": null,
                "esp": null,
            }
        },
        verEdit(){
            let esp = this.dataCourseEdit.esp
            switch(esp){
                case 'CICLO BASICO': this.dataCourseEdit.especialidad='B'; break;
                case 'ELECTROMECANICA': this.dataCourseEdit.especialidad='E'; break;
                case 'CONSTRUCCIONES': this.dataCourseEdit.especialidad='C'; break;
            }
            console.log(this.dataCourseEdit)
        },
        async editCurso(){
            let data = this.dataCourseEdit
            data.role = this.usuario.role
            console.log(data)
            await axios.post('backend/editCourse.php', data)
            .then(resp=>{
                let editcourse = resp.data
                console.log(editcourse)
                if(editcourse.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${editcourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${editcourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }
            })
            .catch(e=>{
                console.log(e)
            })
            dataCourseEdit = {
                "nombre": null,
                "anio": null,
                "div": null,
                "esp": null,
                "especialidad": null,
            }
            this.getCursos()
        },
        async addProfCourse(){
            await this.getUser();
            let data = {
                "idcourse" : this.dataAddProfCourse.course.ID,
                "idusers" : this.dataAddProfCourse.teacher.ID,
                "role" : this.usuario.role
            }
            await axios.post('backend/addTeacherForCourse.php', data)
            .then(resp=>{
                let addTeacherForCourse = resp.data
                console.log(addTeacherForCourse)
                if(addTeacherForCourse.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${addTeacherForCourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${addTeacherForCourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getProfesoresCurso()
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.dataAddProfCourse = {
                "teacher" : null,
                "course" : {
                    "nombre": null,
                    "anio": null,
                    "div": null,
                    "esp": null,
                }
            }
        },
        getProfesoresCurso(){
            axios.get('backend/getTeacherForCourse.php')
            .then(resp=>{
                this.profesores_curso=resp.data
                console.log(this.profesores_curso)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getProfAsig(){
            let idcurso = this.dataAddProfCourse.course.ID
            this.profAsigCurso = this.profesores_curso.filter(item => item.IDcurso === idcurso)
        },
        async getAlumnos(){
            await this.getUser()
            data = {
                "role" : this.usuario.role
            }
            await axios.post('backend/getAlumnos.php',data)
            .then(resp=>{
                this.alumnos = resp.data
                console.log(this.alumnos)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getAlumnosCurso(){
            axios.get('backend/getStudentForCourse.php')
            .then(resp=>{
                this.alumnos_curso=resp.data
                console.log(this.alumnos_curso)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getAlumAsig(){
            let idcurso = this.dataAddAlumCourse.course.ID
            this.alumAsigCurso = this.alumnos_curso.filter(item => item.IDcurso === idcurso)
        },
        async addAlumCourse(){
            await this.getUser();
            let data = {
                "idcourse" : this.dataAddAlumCourse.course.ID,
                "idusers" : this.dataAddAlumCourse.student.ID,
                "role" : this.usuario.role
            }
            await axios.post('backend/addStudentForCourse.php', data)
            .then(resp=>{
                let addStudentForCourse = resp.data
                console.log(addStudentForCourse)
                if(addStudentForCourse.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${addStudentForCourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${addStudentForCourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getAlumnosCurso()
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.dataAddAlumCourse = {
                "student" : null,
                "course" : {
                    "nombre": null,
                    "anio": null,
                    "esp": null,
                }
            }
        },
        getViewCourse(){
            console.log(this.dataCourse)
            let idcurso = this.dataCourse.ID
            this.alumAsigCurso = this.alumnos_curso.filter(item => item.IDcurso === idcurso)
            this.profAsigCurso = this.profesores_curso.filter(item => item.IDcurso === idcurso)
            console.log('Alumnos:',this.alumAsigCurso)
            console.log('Profesores:',this.profAsigCurso)
        },
        async removeUserCourse(){
            console.log(this.dataDeleteUserCourse)
            let data = {
                "ID":this.dataDeleteUserCourse.ID,
                "role":this.dataDeleteUserCourse.role,
                "role_user":this.usuario.role
            }
            await axios.post('backend/removeUserCourse.php', data)
            .then(resp=>{
                console.log(resp.data)
                let removeUserCourse = resp.data
                console.log(removeUserCourse)
                if(removeUserCourse.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${removeUserCourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${removeUserCourse.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.getAlumnosCurso()
            this.getProfesoresCurso()
            $('#verCurso').modal('toggle')
        },
        
            // Metodos de Curricula
            async selectedCourse(curso){
                this.dataSelectedCourse=curso
                this.idxCourse = curso.ID
                console.log(this.dataSelectedCourse)
                console.log(this.idxCourse)
            },
            async agregarUnidad(){
                await this.getUser()
                let unidad = this.unidad
                let data = unidad
                data.role = this.usuario.role
                data.idcourse = this.idxCourse
                console.log(data)
                await axios.post('backend/addUnits.php', data)
                .then(resp=>{
                    let addUnits = resp.data
                    console.log(addUnits)
                    if(addUnits.error){
                        console.log('ERROR!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            ${addUnits.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                    }else{
                        console.log('EXITO!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                            ${addUnits.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                        this.getCurricula()
                    }
                })
                .catch(e=>{
                    console.log(e)
                })
                this.unidad={
                    "idunits":null,
                    "nombre":null,
                    "nro":null,
                    "descripcion":null,
                }
                console.log(this.curricula)
            },
            async agregarActividad(idx){
                await this.getUser()
                let actividad = this.actividad
                let data = actividad
                data.role = this.usuario.role
                data.idunits = this.idunits
                await axios.post('backend/addActivities.php', data)
                .then(resp=>{
                    let addActivities = resp.data
                    console.log(addActivities)
                    if(addActivities.error){
                        console.log('ERROR!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            ${addActivities.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                    }else{
                        console.log('EXITO!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                            ${addActivities.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                        this.getCurricula()
                    }
                })
                .catch(e=>{
                    console.log(e)
                })
                
                this.actividad={
                    "idactivities":null,
                    "nombre":null,
                    "descripcion":null,
                }
                this.idxUnidad = null
                console.log(this.curricula[idx].actividades)
            },
            async agregarRecurso(idxUnidad){
                this.getUser()
                let recurso = this.recurso
                let uploadedError = false
                
                if(recurso.type!='youtube'){
                    this.uploadingFile = true
                    let data = new FormData()
                    data.append("role",this.usuario.role)
                    data.append("idcourse",this.idxCourse)
                    data.append("n_units",this.curricula[idxUnidad].nro)
                    data.append("id_act",this.idactivities)
                    data.append("type",recurso.type)
                    data.append("file",document.getElementById('fileRecurso').files[0])
                    let _this = this
                    _this.file = _this.$refs.fileRecurso.files[0]
                    let name_file = _this.file.name
                    recurso.url = 'files/'+this.idxCourse+'/'+this.curricula[idxUnidad].nro+'/'+this.idactivities+'/'+name_file
                    console.log(data)
                    await axios.post('backend/uploadFile.php', data, {
                        onUploadProgress: uploadedEvent => {
                            let porcentLoad = document.getElementById('porcentLoad')
                            //console.log('Progreso: '+ Math.round(uploadedEvent.loaded / uploadedEvent.total * 100) +' %')
                            porcentLoad.innerHTML=''+Math.round(uploadedEvent.loaded / uploadedEvent.total * 100)+'%'
                        }
                    })
                    .then(resp=>{
                        let uploadedFile = resp.data
                        console.log(uploadedFile)
                        if(uploadedFile.error){
                            console.log('ERROR!')
                            uploadedError=true
                            let alert = document.getElementById('alert')
                            alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                ${uploadedFile.message}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>`
                        }else{
                            console.log('EXITO!')
                            uploadedError=false
                        }
                    })
                    .catch(e=>{
                        console.log(e)
                    })
                }

                let data = recurso
                
                data.idactivities = this.idactivities
                data.role = this.usuario.role

                console.log(data)

                if(!uploadedError){
                    await axios.post('backend/addResources.php', data)
                    .then(resp=>{
                        let addResources = resp.data
                        console.log(addResources)
                        if(addResources.error){
                            console.log('ERROR!')
                            let alert = document.getElementById('alert')
                            alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                ${addResources.message}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>`
                        }else{
                            console.log('EXITO!')
                            let alert = document.getElementById('alert')
                            alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                                ${addResources.message}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>`
                            this.getCurricula()
                        }
                    })
                    .catch(e=>{
                        console.log(e)
                    })
                }
                this.recurso={
                    "url":null,
                    "file":null,
                    "type":null
                }
                this.idxUnidad = null
                this.uploadingFile = false
            },
            nameFileSelect(){
                let name = document.getElementById('fileRecurso').files[0].name;
                let lab = document.getElementById('labelInput')
                lab.innerHTML=name
                console.log('nombre archivo', name)
            },
            async getCurricula(){
                let data = {
                    "idcourse" : this.idxCourse
                }
                axios.post('backend/getCurricula.php', data)
                .then(resp=>{
                    console.log(resp.data)
                    this.curricula = resp.data
                })
                .catch(e=>{
                    console.log(e)
                })
            },
            async deleteRecurso(rec){
                await this.getUser()
                let data = this.dataDeleteRecurso
                data.role = this.usuario.role

                axios.post('backend/deleteResources.php', data)
                .then(resp=>{
                    let deleteResources = resp.data
                    console.log(deleteResources)
                    if(deleteResources.error){
                        console.log('ERROR!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            ${deleteResources.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                    }else{
                        console.log('EXITO!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                            ${deleteResources.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                        this.getCurricula()
                    }                    
                })
                .catch(e=>{
                    console.log(e)
                })
                this.dataDeleteRecurso = null
            },
            async deleteAct(){
                await this.getUser()
                let data = this.dataDeleteActividad
                data.role = this.usuario.role
                data.idcourse = this.idxCourse
                console.log(data)
                await axios.post('backend/deleteActivities.php',data)
                .then(resp=>{
                    let deleteActividad = resp.data
                    console.log(deleteActividad)
                    if(deleteActividad.error){
                        console.log('ERROR!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            ${deleteActividad.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                    }else{
                        console.log('EXITO!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                            ${deleteActividad.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                        this.getCurricula()
                    }
                })
                .catch(e=>{
                    console.log(e)
                })
            },
            async deleteUnits(unit){
                await this.getUser()
                let data = this.dataDeleteUnidad
                data.role = this.usuario.role
                data.idcourse = this.idxCourse

                console.log(data)
                await axios.post('backend/deleteUnits.php',data)
                .then(resp=>{
                    let deleteUnidad = resp.data
                    console.log(deleteUnidad)
                    if(deleteUnidad.error){
                        console.log('ERROR!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            ${deleteUnidad.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                    }else{
                        console.log('EXITO!')
                        let alert = document.getElementById('alert')
                        alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                            ${deleteUnidad.message}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>`
                        this.getCurricula()
                    }
                })
                .catch(e=>{
                    console.log(e)
                })
            },
    },
    created() {
    },
    mounted() {
        this.getCursos()
        this.getProfesoresCurso()
        this.getAlumnosCurso()
    },
    updated() {
        
    },
});