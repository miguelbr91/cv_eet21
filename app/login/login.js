new Vue({
    el: '#login',
    data:{
        login: {
            username:'',
            password:''
        },
        errorLogin: false
    },
    methods: {
        async signIn(){
            let btnLongin = document.getElementById('btnLogin')
            btnLongin.innerHTML=`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Ingresando...`
            console.log(this.login)
            let data = this.login
            await axios.post('backend/signin.php', data)
            .then(resp=>{
                console.log(resp.data)
                let usuario = resp.data
                if(usuario.error){
                    console.log('ERROR!')
                    this.errorLogin=true
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${usuario.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('CONECTADO!')
                    location.reload()
                }
            })
            .catch(e=>{
                console.log(e)
            })
            btnLongin.innerHTML='Ingresar'
        }
    },
    created() {
    },
    mounted() {
    },
});