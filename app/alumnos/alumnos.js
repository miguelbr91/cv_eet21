new Vue({
    el: '#profesores',
    data:{
        title: 'Administración de Alumnos',
        usuario: {},
        alumnos: [],
        nuevoAlum: {
            "nombre":null,
            "apellido":null,
            "dni":null,
            "email":null
        },
        dataEditAlum:{
            "nombre":null,
            "apellido":null,
            "dni":null,
            "email":null
        },
        dataDeletAlum:{
            "nombre":null,
            "apellido":null,
            "dni":null,
            "email":null
        },
    },
    methods: {
        async getUser(){
            await axios.get('backend/session.php')
            .then(resp=>{
                this.usuario=resp.data
                //console.log(this.usuario)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async getAlumnos(){
            await this.getUser()
            data = {
                "role" : this.usuario.role
            }
            await axios.post('backend/getAlumnos.php',data)
            .then(resp=>{
                this.alumnos = resp.data
                console.log(this.alumnos)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async saveAlumno(){
            data = this.nuevoAlum
            data.role = this.usuario.role
            data.idupdater = this.usuario.ID
            console.log('Nuevo Alumno:',data)
            
            await axios.post('backend/addStudent.php', data)
            .then(resp=>{
                let addcstudent = resp.data
                if(addcstudent.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${addcstudent.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${addcstudent.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getAlumnos()
                }
            })
            .catch(e=>{
                console.log(e)
            })

            this.nuevoAlum = {
                "nombre":null,
                "apellido":null,
                "dni":null,
                "email":null
            }
        },
        async editAlumno(){
            await this.getUser
            let data = {
                "ID":this.dataEditAlum.ID,
                "nombre":this.dataEditAlum.nombre,
                "apellido":this.dataEditAlum.apellido,
                "email":this.dataEditAlum.email,
                "dni":this.dataEditAlum.DNI,
                "idupdater": this.usuario.ID,
                "role": this.usuario.role,
            }
            console.log(data)
            axios.post('backend/editStudent.php',data)
            .then(resp=>{
                let editstudent = resp.data
                console.log(editstudent)
                if(editstudent.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${editstudent.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${editstudent.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getAlumnos()
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async deleteAlumno(){
            let data = {
                "ID":this.dataDeletAlum.ID,
                "role": this.usuario.role,
            }
            console.log(data)
            axios.post('backend/deleteStudent.php',data)
            .then(resp=>{
                let deletestudent = resp.data
                console.log(deletestudent)
                if(deletestudent.error){
                    console.log('ERROR!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        ${deletestudent.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                }else{
                    console.log('EXITO!')
                    let alert = document.getElementById('alert')
                    alert.innerHTML=`<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        ${deletestudent.message}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>`
                    this.getAlumnos()
                }
            })
            .catch(e=>{
                console.log(e)
            })
        }
    },
    created() {
    },
    mounted() {
        this.getAlumnos()
    }
});