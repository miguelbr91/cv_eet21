$(document).ready(function(){
    
    fetch('./backend/session.php',{
    method: 'get',
    })
    .then(res=>res.json())
    .then(data=>{
        let usuario=data

        // restriccion de menu de usuario
        if(usuario.role==='ROLE_ADMIN'){
            $('#nav-cursos').addClass('d-block')
            $('#nav-cursos').removeClass('d-none')
            $('#nav-profesores').addClass('d-block')
            $('#nav-profesores').removeClass('d-none')
            $('#nav-alum').addClass('d-block')
            $('#nav-alum').removeClass('d-none')
        }else{
            $('#nav-cursos').addClass('d-none')
            $('#nav-cursos').removeClass('d-block')
            $('#nav-profesores').addClass('d-none')
            $('#nav-profesores').removeClass('d-block')
            $('#nav-alum').addClass('d-none')
            $('#nav-alum').removeClass('d-block')
        }

        // datos del usuario
        let menu_user_name = document.getElementById('name-user-full')
        let navDropdownUser = document.getElementById('navDropdownUser')
        menu_user_name.innerHTML=`<b>${ usuario.nombre } ${ usuario.apellido }</b>`
        navDropdownUser.innerHTML=`<i class="fas fa-user-circle"></i> ${ usuario.nombre }`
    })

    // Adaptar tamaño de imagen al perfil
    let imgProfile = document.getElementById('img-profile')
    let h_img = imgProfile.height
    let w_img = imgProfile.width
    if(w_img>h_img){
        imgProfile.style.height = "200px"
    }else{
        imgProfile.style.width = "200px"
    }
});

function navActive(item){
    switch(item){
        case 'home': $('#nav-home').addClass('active');
                        $('#nav-alum').removeClass('active');
                        $('#nav-cursos').removeClass('active');
                        $('#nav-profesores').removeClass('active');
                        $('#nav-profesores').removeClass('active');
                        $('#nav-miscursos').removeClass('active');
                        break;
        case 'alumnos': $('#nav-home').removeClass('active');
                        $('#nav-alum').addClass('active');
                        $('#nav-cursos').removeClass('active');
                        $('#nav-profesores').removeClass('active');
                        $('#nav-miscursos').removeClass('active');
                        break;
        case 'cursos':  $('#nav-home').removeClass('active');
                        $('#nav-alum').removeClass('active');
                        $('#nav-cursos').addClass('active');
                        $('#nav-profesores').removeClass('active');
                        $('#nav-miscursos').removeClass('active');
                        break;
        case 'profesores':  $('#nav-home').removeClass('active');
                            $('#nav-alum').removeClass('active');
                            $('#nav-cursos').removeClass('active');
                            $('#nav-profesores').addClass('active');
                            $('#nav-miscursos').removeClass('active');
                            break;
        case 'miscursos':   $('#nav-home').removeClass('active');
                            $('#nav-alum').removeClass('active');
                            $('#nav-cursos').removeClass('active');
                            $('#nav-profesores').removeClass('active');
                            $('#nav-miscursos').addClass('active');
                            break;
    }
}

$('#menuHome').click(function(){
    navActive('home');
    $('#content').load('app/home/home.html');
});
$('#menuCursos').click(function(){
    navActive('cursos');
    $('#content').load('app/cursos/cursos.html');
});
$('#menuProf').click(function(){
    navActive('profesores');
    $('#content').load('app/profesores/profesores.html');
});
$('#menuAlum').click(function(){
    navActive('alumnos');
    $('#content').load('app/alumnos/alumnos.html');
});
$('#menuMiscursos').click(function(){
    $('#content').load('app/miscursos/miscursos.html');
    navActive('miscursos');
});


