CREATE SCHEMA IF NOT EXISTS `db_cv` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci;

USE `db_cv`;

/**
*   TABLA DE USUARIOS
*/
CREATE TABLE IF NOT EXISTS `users` (
    `idusers` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL,
    `lastname` VARCHAR(100) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `last_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `role` VARCHAR(45) NOT NULL,
    `last_updated_user` INT NOT NULL,
    `DNI` INT,
    `profile_url` VARCHAR(255),
    PRIMARY KEY (`idusers`),
    FOREIGN KEY (`last_updated_user`) REFERENCES `db_cv`.`users`(`idusers`) ON DELETE NO ACTION ON UPDATE CASCADE,
);

/**
*   AGREGAR CONDICION DE ÍNDICE ÚNICO AL DNI
*/  
ALTER TABLE `users` 
    ADD UNIQUE INDEX `DNI_UNIQUE` (`DNI` ASC) VISIBLE;
;

/**
*   TABLA DE CURSOS/MATERIA
*/
CREATE TABLE IF NOT EXISTS `course` (
    `idcourse` INT NOT NULL AUTO_INCREMENT,
    `name_course` VARCHAR(255) NOT NULL,
    `n_course` INT NOT NULL,
    `s_course` INT NOT NULL,
    PRIMARY KEY (`idcourse`)
);

/**
*   TABLA DE UNIDADES
*/
CREATE TABLE IF NOT EXISTS `units` (
    `idunits` INT NOT NULL AUTO_INCREMENT,
    `idcourse` INT NOT NULL,
    `name_units` VARCHAR(255) NOT NULL,
    `description` VARCHAR(500),
    `n_units` INT NOT NULL,
    PRIMARY KEY (`idunits`),
    FOREIGN KEY (`idcourse`) REFERENCES course(`idcourse`) ON DELETE CASCADE ON UPDATE CASCADE
);

/**
*   TABLA DE ACTIVIDADES
*/
CREATE TABLE IF NOT EXISTS `activities`(
    `idactivities` INT NOT NULL AUTO_INCREMENT,
    `units_idunits` INT NOT NULL,
    `name_activities` VARCHAR(255) NOT NULL,
    `description` VARCHAR(2000),
    PRIMARY KEY (`idactivities`),
    FOREIGN KEY (`units_idunits`) REFERENCES units(`idunits`) ON DELETE CASCADE ON UPDATE CASCADE
);

/**
*   TABLA DE RECURSOS DE ACTIVIDADES
*/
CREATE TABLE IF NOT EXISTS `resources` (
    `idresources` INT NOT NULL AUTO_INCREMENT,
    `idactivities` INT NOT NULL,
    `type` VARCHAR(50) NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`idresources`),
    FOREIGN KEY (`idactivities`) REFERENCES activities(`idactivities`) ON DELETE CASCADE ON UPDATE CASCADE
);

/**
*   TABLA DE RELACION PROFESOR--->CURSO/MATERIA
*/
CREATE TABLE `teacher_course` (
  `idteacher_course` INT NOT NULL AUTO_INCREMENT,
  `idcourse` INT NOT NULL,
  `idusers` INT NOT NULL,
  PRIMARY KEY (`idteacher_course`),
  FOREIGN KEY (`idcourse`) REFERENCES course(`idcourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`idusers`) REFERENCES users(`idusers`) ON DELETE CASCADE ON UPDATE CASCADE
);

/**
*   TABLA DE RELACION ALUMNO--->CURSO/MATERIA
*/
CREATE TABLE `student_course` (
  `idstudent_course` INT NOT NULL AUTO_INCREMENT,
  `idcourse` INT NOT NULL,
  `idusers` INT NOT NULL,
  PRIMARY KEY (`idstudent_course`),
  FOREIGN KEY (`idcourse`) REFERENCES course(`idcourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`idusers`) REFERENCES users(`idusers`) ON DELETE CASCADE ON UPDATE CASCADE
);

/**
*   TABLA DE RELACION UNIDAD--->CURSO/MATERIA
*/
CREATE TABLE `units_course` (
  `idunits_course` INT NOT NULL AUTO_INCREMENT,
  `idcourse` INT NOT NULL,
  `idunits` INT NOT NULL,
  PRIMARY KEY (`idunits_course`),
  FOREIGN KEY (`idcourse`) REFERENCES course(`idcourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`idunits`) REFERENCES units(`idunits`) ON DELETE CASCADE ON UPDATE CASCADE
);

/**
*   VISTA DE PROFESORES
*/
CREATE VIEW vw_profesores AS
	SELECT p.*,u.`name` as name_updater,u.lastname as lastname_updater,u.`role` as role_updater
		FROM users AS p INNER JOIN users as u
        ON p.last_updated_user=u.idusers
        WHERE p.`role`='ROLE_TEACHER';


/**
*   VISTA DE ALUMNOS
*/
CREATE VIEW vw_alumnos AS
	SELECT p.*,u.`name` as name_updater,u.lastname as lastname_updater,u.`role` as role_updater
		FROM users AS p INNER JOIN users as u
        ON p.last_updated_user=u.idusers
        WHERE p.`role`='ROLE_STUDENT';

/**
*   VISTA DE PROFESORES POR CURSO
*/
CREATE VIEW vw_teacherForCourse AS
SELECT 
	txc.*,
    t.`name`,
    t.`lastname`,
    t.`email`,
    t.`role`,
    t.`DNI`,
    c.name_course,
    c.n_course AS CURSO,
    c.s_course AS ESP
	FROM teacher_course as txc INNER JOIN course AS c INNER JOIN users AS t
    ON txc.idcourse=c.idcourse AND txc.idusers=t.idusers;


/**
*   VISTA DE ALUMNOS POR CURSO
*/
CREATE VIEW vw_studentForCourse AS
SELECT 
	sxc.*,
    t.`name`,
    t.`lastname`,
    t.`email`,
    t.`role`,
    t.`DNI`,
    c.name_course,
    c.n_course AS CURSO,
    c.s_course AS ESP
	FROM student_course as sxc INNER JOIN course AS c INNER JOIN users AS t
    ON sxc.idcourse=c.idcourse AND sxc.idusers=t.idusers;

CREATE VIEW vw_unitsForCourse AS
	SELECT 
		c.*,
		u.idunits,
        u.name_units,
        u.description,
        u.n_units
		FROM course AS c INNER JOIN units AS u
		ON c.idcourse = u.idcourse;

CREATE VIEW vw_activitiesForUnits AS
	SELECT 
		a.*
		FROM units AS u INNER JOIN activities AS a
		ON u.idunits = a.units_idunits;


/**
*   INSERTAR DATA INICIAL
*
*       USER: admincv@admin.com
*   PASSWORD: admin1
*
*/
INSERT INTO `users`(`name`,`lastname`,`email`,`password`,`role`,`DNI`) 
    VALUES ('Principal','Administrador','admincv@admin.com','$2y$10$Y9Yv0631.948yIKTrmSNJe01CzuYQLKnmVIWXv7gW7PmC/Uyj1Ejm','ROLE_ADMIN',1);