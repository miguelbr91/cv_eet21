<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $id=$data['ID'];
    
    
    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="DELETE FROM users WHERE idusers=?";
        $delete_sql=$pdo->prepare($sql);
        $delete_sql->execute(array($id));
        if($delete_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha eliminado con éxito el profesor'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha eliminado el profesor'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha eliminado el profesor'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;