<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $rol=$data['role'];
    $respone = [];
    if($rol=='ROLE_ADMIN'){
        $sql="SELECT * FROM `vw_alumnos` ORDER BY `lastname`,`name`";
        $student_sql=$pdo->prepare($sql);
        $student_sql->execute();
        $student=$student_sql->fetchAll();
        for ($i=0; $i < sizeof($student); $i++) { 
            $item = [
                "ID" => $student[$i]['idusers'],
                "nombre" => $student[$i]['name'],
                "apellido" => $student[$i]['lastname'],
                "DNI" => $student[$i]['DNI'],
                "email" => $student[$i]['email'],
                "role" => $student[$i]['role'],
                "ultimaConexion" => $student[$i]['last_connected'],
                "actualizacion" => $student[$i]['last_updated'],
                "IDactualizador" => $student[$i]['last_updated_user'],
                "nombreActualizador" => $student[$i]['name_updater'],
                "apellidoActualizador" => $student[$i]['lastname_updater'],
                "roleActualizador" => $student[$i]['role_updater'],
            ];
            array_push($respone,$item);
        }
    }

    $respone = json_encode($respone);
    
    echo $respone;
