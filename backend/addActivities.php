<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $name_act=$data['nombre'];
    $desc=$data['descripcion'];
    $idunits=$data['idunits'];

    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="INSERT INTO activities(`units_idunits`,`name_activities`,`description`) VALUES (?,?,?)";
        $insert_sql=$pdo->prepare($sql);
        $insert_sql->execute(array($idunits,$name_act,$desc));
        if($insert_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha registrado con éxito la nueva actividad'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha registrado la nueva actividad'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha registrado la nueva actividad'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;