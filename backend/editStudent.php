<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $idupdater=intval($data['idupdater']);
    $last_updated=date('Y-m-d H:i:s');
    $idstudent=intval($data['ID']);
    $name=$data['nombre'];
    $lastname=$data['apellido'];
    $dni=intval($data['dni']);
    $email=$data['email'];

    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="UPDATE users SET `name`=?,`lastname`=?,`email`=?,`last_updated`=?,`last_updated_user`=?,`DNI`=? WHERE `idusers`=?";
        $student_sql=$pdo->prepare($sql);
        $student_sql->execute(array($name,$lastname,$email,$last_updated,$idupdater,$dni,$idstudent));
        if($student_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha editado con éxito los datos del alumno'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha podido editar los datos del alumno'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha podido editar los datos del alumno'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;