<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $rol=$data['role'];
    $respone = [];
    if($rol=='ROLE_ADMIN'){
        $sql="SELECT * FROM `vw_profesores` ORDER BY `lastname`,`name`";
        $teacher_sql=$pdo->prepare($sql);
        $teacher_sql->execute();
        $teacher=$teacher_sql->fetchAll();
        for ($i=0; $i < sizeof($teacher); $i++) { 
            $item = [
                "ID" => $teacher[$i]['idusers'],
                "nombre" => $teacher[$i]['name'],
                "apellido" => $teacher[$i]['lastname'],
                "DNI" => $teacher[$i]['DNI'],
                "email" => $teacher[$i]['email'],
                "role" => $teacher[$i]['role'],
                "ultimaConexion" => $teacher[$i]['last_connected'],
                "actualizacion" => $teacher[$i]['last_updated'],
                "IDactualizador" => $teacher[$i]['last_updated_user'],
                "nombreActualizador" => $teacher[$i]['name_updater'],
                "apellidoActualizador" => $teacher[$i]['lastname_updater'],
                "roleActualizador" => $teacher[$i]['role_updater'],
            ];
            array_push($respone,$item);
        }
    }

    $respone = json_encode($respone);
    
    echo $respone;
