<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $type = $data['type'];
    $url = $data['url'];
    $idactivities = intval($data['idactivities']);

    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="INSERT INTO resources(`idactivities`,`type`,`url`) VALUES (?,?,?)";
        $insert_sql=$pdo->prepare($sql);
        $insert_sql->execute(array($idactivities,$type,$url));
        if($insert_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha registrado con éxito el nueva recurso'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha registrado el nueva recurso'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha registrado el nueva recurso'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;