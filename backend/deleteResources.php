<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $idrecurso=$data['idresources'];
    $tipo=$data['type'];
    $link='../'.$data['url'];
    
    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        
        if($tipo!='youtube'){
            // borrar el archivo local
            unlink($link);
        }

        $sql="DELETE FROM resources WHERE idresources=?";
        $delete_sql=$pdo->prepare($sql);
        $delete_sql->execute(array($idrecurso));
        if($delete_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha eliminado con éxito el alumno'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha eliminado el alumno'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha eliminado el alumno'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;