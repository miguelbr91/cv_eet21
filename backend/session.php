<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    session_start();
    $usr = $_SESSION['ID'];
    
    if(!empty($usr)){
        $usr = [
            "ID" => $_SESSION['ID'],
            "nombre" => $_SESSION['nombre'],
            "apellido" => $_SESSION['apellido'],
            "email" => $_SESSION['email'],
            "role" => $_SESSION['role'],
        ];
        
    }else{
        $usr = ["ID" => $_SESSION['ID']];
    }
    echo json_encode($usr);
?>