<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $idupdater=intval($data['idupdater']);
    $name=$data['nombre'];
    $lastname=$data['apellido'];
    $dni=$data['DNI'];
    $email=$data['email'];
    $passEncrypt=password_hash($dni, PASSWORD_DEFAULT);
    $role='ROLE_TEACHER';

    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="INSERT INTO users(`name`,`lastname`,`email`,`password`,`role`,`last_updated_user`,`DNI`) VALUES (?,?,?,?,?,?,?)";
        $teacher_sql=$pdo->prepare($sql);
        $teacher_sql->execute(array($name,$lastname,$email,$passEncrypt,$role,$idupdater,$dni));
        if($teacher_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha registrado con éxito el nuevo profesor'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha registrado el nuevo profesor'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha registrado el nuevo profesor'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;