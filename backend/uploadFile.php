<?php
    // Setting php.ini
    // ini_set('post_max_size','250M');  // Tamaño máximo de datos enviados por método POST.
    // ini_set('upload_max_filesize','250M');   // Tamaño máximo para subir archivos al servidor.
    // ini_set('max_execution_time','1000');  // Tiempo máximo de ejecución de éste script en segundos.
    // ini_set('max_input_time','1000'); /*Tiempo máximo en segundos que el script puede usar para analizar los datos input, sean post,get o archivos.*/
    // ini_set("memory_limit" , "250M") ; /*Tamaño máximo que el script puede usar de la memoria, mientras se ejecuta.*/
    // set_time_limit(0); /* Tiempo máximo en segundos, que puede el script estar ejecutándose. El cero, da tiempo ilimitado.*/
    
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');


    $rol=$_POST['role'];
    $archivo=$_FILES['file']['name'];
    $courseID=$_POST['idcourse'];
    $unitsNro=$_POST['n_units'];
    $actNro=$_POST['id_act'];
    $target_dir="../files/$courseID/$unitsNro/$actNro/";
    $target_file=$target_dir.basename($archivo);
    
    mkdir($target_dir, 0777, true);
    
    if($_FILES['file']['error']){
        $response=[
            "error" => true,
            "message" => "No se ha guardado el archivo.",
            "type" => $_FILES['file']['error']
        ];
    }else if(move_uploaded_file($_FILES['file']['tmp_name'],$target_file)){
        $response=[
            "error" => false,
            "message" => "Archivo guardado."
        ];
    }else{
        $response=[
            "error" => true,
            "message" => "No se ha guardado el archivo."
        ];
    }

    echo json_encode($response);