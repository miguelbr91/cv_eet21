<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role_user'];
    $id=$data['ID'];
    $rol_delete=$data['role'];
    
    
    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        if($rol_delete=='ROLE_TEACHER'){
            $sql="DELETE FROM teacher_course WHERE idteacher_course=?";
            $usr='Profesor';
        }else if($rol_delete=='ROLE_STUDENT'){
            $sql="DELETE FROM student_course WHERE idstudent_course=?";
            $usr='Alumno';
        }
        $delete_sql=$pdo->prepare($sql);
        $delete_sql->execute(array($id));
        if($delete_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha quitado con éxito el '.$usr
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha quitado el '.$usr
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha quitado el '.$usr
        ];
    }

    $respone = json_encode($respone);

    echo $respone;