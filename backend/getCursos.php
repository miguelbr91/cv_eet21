<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $rol=$data['role'];
    $respone = [];
    if($rol=='ROLE_ADMIN'){
        $sql="SELECT * FROM course ORDER BY `s_course`,`n_course`";
        $course_sql=$pdo->prepare($sql);
        $course_sql->execute();
        $course=$course_sql->fetchAll();
        for ($i=0; $i < sizeof($course); $i++) { 
            $item = [
                "ID" => $course[$i]['idcourse'],
                "nombre" => $course[$i]['name_course'],
                "anio" => $course[$i]['n_course'],
                "esp" => $course[$i]['s_course']
            ];
            array_push($respone,$item);
        }
    }else if($rol=='ROLE_TEACHER'){
        $idteacher=intval($data['idusers']);
        $sql="SELECT * FROM vw_teacherForCourse WHERE idusers=? ORDER BY `ESP`,`CURSO`";
        $course_sql=$pdo->prepare($sql);
        $course_sql->execute(array($idteacher));
        $course=$course_sql->fetchAll();
        for ($i=0; $i < sizeof($course); $i++) { 
            $item = [
                "ID" => $course[$i]['idcourse'],
                "nombre" => $course[$i]['name_course'],
                "anio" => $course[$i]['CURSO'],
                "esp" => $course[$i]['ESP']
            ];
            array_push($respone,$item);
        }
    }else if($rol=='ROLE_STUDENT'){
        $idstudent=intval($data['idusers']);
        $sql="SELECT * FROM vw_studentForCourse WHERE idusers=? ORDER BY `ESP`,`CURSO`";
        $course_sql=$pdo->prepare($sql);
        $course_sql->execute(array($idstudent));
        $course=$course_sql->fetchAll();
        for ($i=0; $i < sizeof($course); $i++) { 
            $item = [
                "ID" => $course[$i]['idcourse'],
                "nombre" => $course[$i]['name_course'],
                "anio" => $course[$i]['CURSO'],
                "esp" => $course[$i]['ESP']
            ];
            array_push($respone,$item);
        }
    }

    $respone = json_encode($respone);
    
    echo $respone;
