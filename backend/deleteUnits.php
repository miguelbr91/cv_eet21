<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $idunidad=$data['idunits'];
    $idcurso=$data['idcourse'];
    $nro_unidad=$data['nro'];
    
    $respone = [];
    
    if($rol=='ROLE_ADMIN'){

        function deleteDirectory($dir) {
            if(!$dh = @opendir($dir)) return;
            while (false !== ($current = readdir($dh))) {
                if($current != '.' && $current != '..') {
                    //echo 'Se ha borrado el archivo '.$dir.'/'.$current.'<br/>';
                    if (!@unlink($dir.'/'.$current)) 
                        deleteDirectory($dir.'/'.$current);
                }       
            }
            closedir($dh);
            // echo 'Se ha borrado el directorio '.$dir.'<br/>';
            @rmdir($dir);
        }
        
        $midir = "../files/$idcurso/$nro_unidad";
        
        deleteDirectory($midir);

        $sql="DELETE FROM units WHERE idunits=?";
        $delete_sql=$pdo->prepare($sql);
        $delete_sql->execute(array($idunidad));
        if($delete_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha eliminado con éxito la Unidad N°'.$nro_unidad
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha eliminado la Unidad N°'.$nro_unidad
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha eliminado la Unidad N°'.$nro_unidad
        ];
    }

    $respone = json_encode($respone);

    echo $respone;