<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $name_units=$data['nombre'];
    $nro_units=$data['nro'];
    $desc=$data['descripcion'];
    $idcourse=intval($data['idcourse']);

    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="INSERT INTO units(`idcourse`,`name_units`,`description`,`n_units`) VALUES (?,?,?,?)";
        $insert_sql=$pdo->prepare($sql);
        $insert_sql->execute(array($idcourse,$name_units,$desc,$nro_units));
        if($insert_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha registrado con éxito la nueva unidad'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha registrado la nueva unidad'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha registrado la nueva unidad'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;