<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";

    $idcourse=intval($data['idcourse']);

    $respone = [];
    
    $sql="SELECT * FROM vw_unitsForCourse WHERE idcourse=? ORDER BY s_course,n_course,n_units";
    $select_sql=$pdo->prepare($sql);
    $select_sql->execute(array($idcourse));
    
    $units_course=$select_sql->fetchAll();
    $list_units=[];
    for ($i=0; $i < sizeof($units_course) ; $i++) { 
        $unit = $units_course[$i];
        $unidad = [
            "idunits" => $unit['idunits'],
            "nombre" => $unit['name_units'],
            "nro" => intval($unit['n_units']),
            "descripcion" => $unit['description'],
            "actividades" => []
        ];
        $idunits=$unit['idunits'];
        $sql="SELECT * FROM vw_activitiesForUnits WHERE units_idunits=?";
        $select_sql2=$pdo->prepare($sql);
        $select_sql2->execute(array($idunits));
        $actividades = $select_sql2->fetchAll();

        for ($j=0; $j < sizeof($actividades); $j++) { 
            $actividad = $actividades[$j];
            $elto=[
                "idactivities" => $actividad['idactivities'],
                "nombre" => $actividad['name_activities'],
                "descripcion" => $actividad['description'],
                "recursos" => []
            ];
            $idactivities=$elto['idactivities'];
            $sql="SELECT * FROM resources WHERE idactivities=?";
            $select_sql3=$pdo->prepare($sql);
            $select_sql3->execute(array($idactivities));
            $recursos = $select_sql3->fetchAll();
            for ($k=0; $k < sizeof($recursos); $k++) { 
                $recurso = $recursos[$k];
                $item=[
                    "idresources" => $recurso['idresources'],
                    "type" => $recurso['type'],
                    "url" => $recurso['url']
                ];
                array_push($elto['recursos'],$item);
            }
            array_push($unidad['actividades'],$elto);
        }
        array_push($list_units,$unidad);
    }
    
    $respone = json_encode($list_units);

    echo $respone;