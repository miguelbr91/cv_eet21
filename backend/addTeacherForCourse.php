<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $idcourse=intval($data['idcourse']);
    $idusers=intval($data['idusers']);
    
    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="INSERT INTO teacher_course(`idcourse`,`idusers`) VALUES (?,?)";
        $course_sql=$pdo->prepare($sql);
        $course_sql->execute(array($idcourse,$idusers));
        if($course_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha asignado con éxito el nuevo profesor del curso'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha asignado el profesor del curso'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha asignado el profesor del curso'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;