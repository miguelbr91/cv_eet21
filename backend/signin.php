<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $user_name=$data['username'];
    $user_pass=$data['password'];
    include_once "conn.php";
    $sql="SELECT * FROM users WHERE `email`=?";
    $sesion_sql=$pdo->prepare($sql);
    $sesion_sql->execute(array($user_name));
    $sesion_user=$sesion_sql->fetch();
    if(!empty($sesion_user)){
        if(password_verify($user_pass,$sesion_user['password'])){
            $usuario=[
                "ID" => $sesion_user['idusers'],
                "nombre" => $sesion_user['name'],
                "apellido" => $sesion_user['lastname'],
                "email" => $sesion_user['email'],
                "role" => $sesion_user['role'],
                "error" => false
            ];
            session_start();
            $_SESSION['ID']=$usuario['ID'];
            $_SESSION['nombre']=$usuario['nombre'];
            $_SESSION['apellido']=$usuario['apellido'];
            $_SESSION['email']=$usuario['email'];
            $_SESSION['role']=$usuario['role'];

            $last_connected=date('Y-m-d H:i:s');
            $sql="UPDATE users SET `last_connected`=? WHERE `idusers`=?";
            $update_sesion_sql=$pdo->prepare($sql);
            $update_sesion_sql->execute(array($last_connected,$usuario['ID']));
            
            echo json_encode($usuario);
        }else{
            echo json_encode(["error"=>true,"message"=>"<strong>Error!</strong> Contraseña incorrecta."]);
        }
    }else{
        echo json_encode(["error"=>true,"message"=>"<strong>Error!</strong> El usuario no existe."]);
    }
?>