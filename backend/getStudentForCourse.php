<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";

    $respone = [];

    $sql="SELECT * FROM `vw_studentForCourse` ORDER BY `ESP`,`CURSO`,`lastname`,`name`";
    $teacher_course_sql=$pdo->prepare($sql);
    $teacher_course_sql->execute();
    $teacher=$teacher_course_sql->fetchAll();
    for ($i=0; $i < sizeof($teacher); $i++) { 
        $item = [
            "ID" => $teacher[$i]['idstudent_course'],
            "IDalumno" => $teacher[$i]['idusers'],
            "nombre" => $teacher[$i]['name'],
            "apellido" => $teacher[$i]['lastname'],
            "DNI" => $teacher[$i]['DNI'],
            "email" => $teacher[$i]['email'],
            "role" => $teacher[$i]['role'],
            "IDcurso" => $teacher[$i]['idcourse'],
            "materia" => $teacher[$i]['name_course'],
            "curso" => $teacher[$i]['CURSO'],
            "esp" => $teacher[$i]['ESP'],
        ];
        array_push($respone,$item);
    }

    $respone = json_encode($respone);
    
    echo $respone;