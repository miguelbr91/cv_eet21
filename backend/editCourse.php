<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $rol=$data['role'];
    $id=$data['ID'];
    $name=$data['nombre'];
    $anio=$data['anio'];
    $esp=$data['especialidad'];
    switch($esp){
        case 'B': $especialidad='CICLO BASICO'; break;
        case 'C': $especialidad='CONSTRUCCIONES'; break;
        case 'E': $especialidad='ELECTROMECANICA'; break;
    }
    
    $respone = [];
    
    if($rol=='ROLE_ADMIN'){
        $sql="UPDATE course SET `name_course`=?,`n_course`=?,`s_course`=? WHERE idcourse=?";
        $course_sql=$pdo->prepare($sql);
        $course_sql->execute(array($name,$anio,$especialidad,$id));
        if($course_sql){
            //message success
            $respone = [
                "error" => false,
                "message" => '<strong>Correcto!</strong> Se ha registrado con éxito el nuevo curso'
            ];
        }else{
            //message error
            $respone = [
                "error" => true,
                "message" => '<strong>Error!</strong> No se ha registrado el nuevo curso'
            ];
        }
    }else{
        $respone = [
            "error" => true,
            "message" => '<strong>Error!</strong> No se ha registrado el nuevo curso'
        ];
    }

    $respone = json_encode($respone);

    echo $respone;