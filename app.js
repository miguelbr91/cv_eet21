$(document).ready(function(){
    fetch('backend/session.php',{
       method: 'get',
    })
    .then(res=>res.json())
    .then(data=>{
        console.log(data)
        if(data.ID===null){
            $('#content').load('app/login/login.html')
        }else{
            $('#menu').load('app/navbar/navbar.html');
            $('#content').load('app/home/home.html')
        }
    })
});

